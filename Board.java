public class Board{
	private Square[][] tictactoeBoard;
	
		//tictactoeBoard = new Square[3][3];
	
	public Board(){	
		tictactoeBoard = new Square[3][3];
		for(int i = 0; i < tictactoeBoard.length; i++){
			for(int j = 0; j < tictactoeBoard[i].length; j++){
				tictactoeBoard[i][j] = Square.BLANK;
			}
		}
		//return tictactoeBoard;
	}
	
	public String toString(){
		String string = "";
		for(int i = 0; i < tictactoeBoard.length; i++){
			for(int j = 0; j < tictactoeBoard[i].length; j++){
				string += tictactoeBoard[i][j] + ", ";
			}
			string += "\n";
		}
		return string;
	}
	
	public boolean placeToken (int row, int col, Square playerToken){
		if(row > 2 || col > 2){
			return false;
		}
		if(row<0 || col<0){
			return false;
		}
		
		if(tictactoeBoard[row][col] == Square.BLANK){
			tictactoeBoard[row][col] = playerToken;
			return true;
		}
		else{
			return false;
		}
	}
	
	public boolean checkIfFull (){
		for(int i = 0; i < tictactoeBoard.length; i++){
			for (int j = 0; j < tictactoeBoard[i].length; j++){
				if(tictactoeBoard[i][j] == Square.BLANK){
					return false;
				}
			}
		}
		return true;
	}
	
	private boolean checkIfWinningHorizontal(Square playerToken){
		for(int i = 0; i < tictactoeBoard.length; i++){
			for (int j = 0; j < tictactoeBoard[i].length; j++){
				if(tictactoeBoard[i][0] == playerToken && tictactoeBoard[i][1] == playerToken && tictactoeBoard[i][2] == playerToken){
					return true;
				}
			}
		}
		return false;
	}
	
	private boolean checkIfWinningVertical(Square playerToken){
		for(int i = 0; i < tictactoeBoard.length; i++){
			for (int j = 0; j < tictactoeBoard[i].length; j++){
				if(tictactoeBoard[0][i] == playerToken && tictactoeBoard[1][i] == playerToken && tictactoeBoard[2][i] == playerToken){
					return true;
				}
			}
		}
		return false;
	}
	
	private boolean checkIfWinningDiagonal(Square playerToken){
		if (playerToken == tictactoeBoard[0][0] && playerToken == tictactoeBoard[1][1] && playerToken == tictactoeBoard[2][2]){
			return true;
		}
		if (playerToken == tictactoeBoard[0][2] && playerToken == tictactoeBoard[1][1] && playerToken == tictactoeBoard[2][0]){
			return true;
		}
		return false;
	}
	
	public boolean checkIfWinning(Square playerToken){
		if(checkIfWinningHorizontal(playerToken) == true ||checkIfWinningVertical(playerToken) || checkIfWinningDiagonal(playerToken)){
		return true;
		}
		else{
			return false;
		}
	}
	
}