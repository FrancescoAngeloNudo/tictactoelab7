import java.util.Scanner;
public class TicTacToeGame{
	
	public static void main (String[]args){
		Scanner s = new Scanner(System.in);
		System.out.println("Welcome to Tic Tac Toe!");
		Board b = new Board();
		boolean gameOver = false;
		int player = 1;
		Square playerToken = Square.X;
		
		while(gameOver == false){
			System.out.println(b);
			if(player == 1){
				playerToken = Square.X;
			}
			else{
				playerToken = Square.O;
			}
			
			System.out.println("Row");
			int row = s.nextInt() - 1;
			System.out.println("Column");
			int col = s.nextInt() - 1;
			
			boolean valid = b.placeToken(row,col,playerToken);
			
			while(!valid){
			System.out.println("Re-enter Row");
			row = s.nextInt() - 1;
			System.out.println("Re-enter Column");
			col = s.nextInt() -1;
			valid = b.placeToken(row,col,playerToken);
			}
			
			if(b.checkIfFull() == true){
				System.out.println("It's a Tie!");
			}
			else if(b.checkIfWinning(playerToken) == true){
				System.out.println("Player " + player + " Wins!");
				gameOver = true;
			}
			else{
				player++;
				if(player > 2){
					player = 1;
				}
			}
		}
	}
}